import 'zone.js/dist/zone-testing';
import {ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {HttpClient, HttpHandler} from '@angular/common/http';

describe('AppComponent', () => {

  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppComponent],
      providers: [FormsModule, HttpClient, HttpHandler]
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should send the text to detect the change on countrySearchInput', fakeAsync((done) => {
    component.countrySearchInputUpdate.subscribe(valueReceived => {
      expect(valueReceived).toEqual('Changed text');
      done();
    });
    component.countrySearchInput = 'Changed text';
    fixture.detectChanges();
  }));

  it('should send the text to detect the change on deviceSearchInput', fakeAsync((done) => {
    component.deviceSearchInputUpdate.subscribe(valueReceived => {
      expect(valueReceived).toEqual('Changed text');
      done();
    });
    component.deviceSearchInput = 'Changed text';
    fixture.detectChanges();
  }));
});

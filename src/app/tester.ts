export class Tester {
  name: string;
  bugs: number;

  constructor(name, bugs) {
    this.name = name;
    this.bugs = bugs;
  }
}

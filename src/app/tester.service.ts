import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Tester} from './tester';
import {map} from 'rxjs/operators';

import {environment} from 'src/environments/environment';

@Injectable({providedIn: 'root'})
export class TesterService {

  constructor(private http: HttpClient) {
  }

  fetchTesters(countries: string[], devices: string[]): Observable<Tester[]> {
    let searchParams = new HttpParams();
    let httpHeaders = new HttpHeaders();
    searchParams = searchParams.append('country', countries.toString());
    searchParams = searchParams.append('device', devices.toString());
    httpHeaders = httpHeaders.append('Access-Control-Allow-Origin', '*');
    return this.http.get(environment.apiUrl,
      {
        params: searchParams,
        headers: httpHeaders
      }
    ).pipe(map((testers: Tester[]) => testers));
  }
}

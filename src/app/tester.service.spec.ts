import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TesterService} from './tester.service';
import {Tester} from './tester';
import {HttpClient} from '@angular/common/http';

describe('TesterService', () => {

  let testerService: TesterService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TesterService, HttpClient],
    });
    testerService = TestBed.inject(TesterService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('fetchTesters() should return dummy data', () => {
    const dummyResponse = [new Tester('User1', '10'), new Tester('User2', '5')];
    testerService.fetchTesters(['ALL'], ['ALL']).subscribe((response) => {
      expect(response).toEqual(dummyResponse);
    });

    const request = httpMock.expectOne('http://localhost:8080/testers?country=ALL&device=ALL');
    expect(request.request.method).toBe('GET');
    request.flush(dummyResponse);
  });
});

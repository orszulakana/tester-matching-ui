import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Tester} from './tester';
import {TesterService} from './tester.service';
import {debounceTime, distinctUntilChanged, filter} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  isSearching: boolean;
  loadedTesters: Tester[] = [];
  error = null;

  public countrySearchInput: string;
  countrySearchInputUpdate = new Subject<string>();
  public deviceSearchInput: string;
  deviceSearchInputUpdate = new Subject<string>();

  constructor(private http: HttpClient, private testerService: TesterService) {
    this.isSearching = false;
    this.loadedTesters = [];
  }

  ngOnInit(): void {
    this.countrySearchInputUpdate.pipe(
      filter(value => value.length >= 2),
      debounceTime(1000),
      distinctUntilChanged())
      .subscribe(country => {
        this.isSearching = true;
        this.searchTesters(country, this.getDeviceInput());
      });

    this.deviceSearchInputUpdate.pipe(
      filter(value => value.length >= 2),
      debounceTime(1000),
      distinctUntilChanged())
      .subscribe(device => {
        this.isSearching = true;
        this.searchTesters(this.getCountryInput(), device);
      });
  }

  searchTesters(country: string, device: string): void {
    const countries = country.split(',').map((item: string) => item.trim());
    const devices = device.split(',').map((item: string) => item.trim());

    this.testerService.fetchTesters(countries, devices).subscribe(
      testers => {
        this.isSearching = false;
        this.loadedTesters = testers;
      },
      error => {
        this.isSearching = false;
        this.error = error.message;
      }
    );
  }

  getDeviceInput(): string {
    return this.deviceSearchInput === undefined ? 'ALL' : this.deviceSearchInput;
  }

  getCountryInput(): string {
    return this.countrySearchInput === undefined ? 'ALL' : this.countrySearchInput;
  }
}
